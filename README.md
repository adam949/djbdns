djbdns is a nameserver written by djb because he was tired of seeing
vulnerabilities in Bind. [Official website](https://cr.yp.to/djbdns.html)

This is an unofficial repo that took the code available and put it into git,
added scripts to build a Debian package for it, and added CI to make building
that package as easy as possible.

To build the package from source:  
  sudo apt install -y debhelper dpkg-dev  
  dpkg-buildpackage -b --no-sign
